# MREST client NodeJS
An example Javascript client for a heavily structured (Model) REST API. It should be suitable for use with any valid json schema.

This project extends the [Unirest](http://unirest.io/) client. It also uses [json schema](http://json-schema.org/) to specify models and validate data before and after sending.

For more information about MREST, please visit its [wiki](https://bitbucket.org/deginner/flask-mrest/wiki/Home).

## Usage
Initialize the client:

```
var mrest_client = require("../client.js");

// The example schema included with the Flask MREST server
var COIN_SCHEMA = {
    "title": "Coin Schema",
    "type": "object",
    "properties": {
        "metal": {
            "type": "string"
        },
        "mint": {
            "type": "string"
        }
    },
    "required": ["metal", "mint"]
};

// Default to localhost configuration and example host
var cfg = {"url": "http://0.0.0.0:8002", "host": "example.com", "schemas": {"coin": COIN_SCHEMA}};
var mrest = mrest_client(cfg);
```

Create a new Coin:


```
var coin;

mrest.post('coin', {'metal':'PB','mint':'perhaps my basement?'}, function(err, resp) {
  coin = resp;
});

```

Get the Coin:

`
mrest.get('coin', coin.id, function(err, resp) {});
`

Update the Coin:

`
mrest.put('coin', coin.id, {'metal':'PB': 'pirate booty'}, function(err, resp) {});
`


Delete the Coin:

`
mrest.delete('coin', coin.id, function(err, resp) {});
`

## Example Server
An example MREST server (used for tests) can be found [here](https://bitbucket.org/deginner/flask-mrest). Note that it requires Python 2.7 and uses [Flask](http://flask.pocoo.org/).
