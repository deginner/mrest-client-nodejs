var fs = require('fs');
var path = require('path');
var should = require('should');
var auth = require("../auth.js");
var bitcore = require('bitcore');
var bitmess = require('bitcore-message');

var privKey = new bitcore.PrivateKey();
var pubKey = privKey.toPublicKey();
var pubHash = privKey.toAddress().toString();

describe('mrest_auth', function() {
  it('encode_compact_signed_message', function(done) {
    var pm = {'method': 'GET', 'data': 'eyJpZCI6ImN1cGNha2VzIn0=', 'headers': {}, 'model': 'dulces'};
    var packed_message = auth.encode_compact_signed_message("GET", {'id': 'cupcakes'}, {}, 'dulces');
    packed_message.should.have.property('method');
    packed_message.should.have.property('data');
    packed_message.should.have.property('headers');
    packed_message.should.have.property('model');
    should.deepEqual(pm, packed_message);
    done();
  });
  it('encode_compact_signed_message_auth_style', function(done) {
    var pm = {'method': 'GET', 'data': 'eyJpZCI6ImN1cGNha2VzIn0=', 'headers': {}, 'model': 'dulces'};
    var packed_message = auth.encode_compact_signed_message("GET",
            auth.encode_signed_message({'id': 'cupcakes'}), {}, 'dulces');
    packed_message.should.have.property('method');
    packed_message.should.have.property('data');
    packed_message.should.have.property('headers');
    packed_message.should.have.property('model');
    should.deepEqual(pm, packed_message);
    done();
  });

  it('encode_signed_message', function(done) {
    var am = {'data': 'eyJpZCI6ImN1cGNha2VzIn0='};
    var auth_message = auth.encode_signed_message({'id': 'cupcakes'});
    auth_message.should.have.property('data');
    should.deepEqual(am, auth_message);
    done();
  });
  it('encode_signed_message_string', function(done) {
    var am = {'data': 'eyJpZCI6ImN1cGNha2VzIn0='};
    var auth_message = auth.encode_signed_message(JSON.stringify({'id': 'cupcakes'}));
    auth_message.should.have.property('data');
    should.deepEqual(am, auth_message);
    done();
  });

  it('decode_signed_message', function(done) {
    var encoded_message = JSON.stringify({'data': 'eyJpZCI6ImN1cGNha2VzIn0='});
    var decoded_message = auth.decode_signed_message(encoded_message);
    decoded_message.should.have.property('id');
    should.deepEqual(decoded_message, {'id': 'cupcakes'});
    done();
  });
  it('decode_signed_message_object', function(done) {
    var encoded_message = {'data': 'eyJpZCI6ImN1cGNha2VzIn0='};
    var decoded_message = auth.decode_signed_message(encoded_message);
    decoded_message.should.have.property('id');
    should.deepEqual(decoded_message, {'id': 'cupcakes'});
    done();
  });

  it('create_sign_str', function(done) {
    var now = "1438308764326";
    var mess = 'eyJpZCI6ImN1cGNha2VzIn0=';
    var ss = 'eyJpZCI6ImN1cGNha2VzIn0=GET1438308764326';
    var signed_str = auth.create_sign_str(mess, 'get', now);
    signed_str.should.equal(ss);
    done();
  });

  it('sign_message', function(done) {
    var mess = auth.encode_signed_message({'id': 'cupcakes'});
    var signstr = auth.create_sign_str(mess.data, 'get', "1438308764326");
    var Mess = new bitmess(signstr);
    var sign = auth.sign_message(mess.data, 'get', "1438308764326", privKey);
    Mess.verify(pubHash, sign).should.be.true;
    done();
  });

  it('prepare_mrest_message_authenticate', function(done) {
    var sdata = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash, privKey, {}, ['authenticate'])
    sdata.should.have.property('data')
    sdata.should.have.property('headers')
    sdata.headers.should.have.property('x-mrest-sign-0')
    sdata.headers.should.have.property('x-mrest-time-0')
    sdata.headers.should.have.property('x-mrest-pubhash-0')
    done();
  });
  it('prepare_mrest_message_pubhash', function(done) {
    var sdata = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash,
        privKey, {}, ['pubhash'])
    sdata.should.have.property('data')
    should.deepEqual(sdata.data, {'id': 'cupcakes'});
    sdata.should.have.property('headers')
    sdata.headers.should.not.have.property('x-mrest-sign-0')
    sdata.headers.should.not.have.property('x-mrest-time-0')
    sdata.headers.should.have.property('x-mrest-pubhash-0')
    done();
  });
  it('prepare_mrest_message_defaults', function(done) {
    var sdata = auth.prepare_mrest_message('get', {'id': 'cupcakes'})
    sdata.should.have.property('data')
    should.deepEqual(sdata.data, {'id': 'cupcakes'});
    sdata.should.have.property('headers')
    sdata.headers.should.not.have.property('x-mrest-sign')
    sdata.headers.should.not.have.property('x-mrest-time')
    sdata.headers.should.not.have.property('x-mrest-pubhash')
    done();
  });

  it('verify_message', function(done) {
    var sdata = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash, privKey, {}, ['authenticate'])
    var mess = sdata.data.data;
    var signatures = auth.verify_message(mess, sdata.headers, [pubHash], 'get', [pubHash])
    signatures.should.have.property(pubHash);
    signatures[pubHash].should.equal(sdata.headers['x-mrest-sign-0']);
    done();
  });
  it('verify_message_bad_sign', function(done) {
    var privKey2 = new bitcore.PrivateKey();
    var sdata = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash, privKey2, {}, ['authenticate'])
    var mess = sdata.data.data;
    ( function() {
        auth.verify_message(mess, sdata.headers, [pubHash], 'get', [pubHash])}
    ).should.throw('Signature verification failed');
    done();
  });
  it('verify_message_bad_message', function(done) {
    var sdata = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash, privKey, {}, ['authenticate'])
    var mess = auth.encode_signed_message({'id': 'flan de coco'});
    ( function() {
        auth.verify_message(mess, sdata.headers, [pubHash], 'get', [pubHash])}
    ).should.throw('Signature verification failed');
    done();
  });
  it('verify_message_untrusted_signer', function(done) {
    var privKey2 = new bitcore.PrivateKey();
    var pubHash2 = privKey2.toAddress().toString();
    var sdata = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash, privKey, {}, ['authenticate'])
    var mess = sdata.data.data;
    ( function() {
        auth.verify_message(mess, sdata.headers, [pubHash2], 'get', [pubHash])}
    ).should.throw('Unauthorized pubhash ' + pubHash);
    done();
  });
  it('verify_message_required_signer', function(done) {
    var privKey2 = new bitcore.PrivateKey();
    var pubHash2 = privKey2.toAddress().toString();
    var sdata = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash, privKey, {}, ['authenticate'])
    var mess = sdata.data.data;
    ( function() {
        auth.verify_message(mess, sdata.headers, [pubHash], 'get', [pubHash, pubHash2])}
    ).should.throw('expected 2 signatures but only found 1');
    done();
  });

  it('verify_message_multiple_authorized_required_signers', function(done) {
    var privKey2 = new bitcore.PrivateKey();
    var pubHash2 = privKey2.toAddress().toString();
    var sdata = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash, privKey, {}, ['authenticate'])
    var sdata2 = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash2, privKey2, {}, ['authenticate'])
    sdata.headers['x-mrest-sign-1'] = sdata2.headers['x-mrest-sign-0'];
    sdata.headers['x-mrest-time-1'] = sdata2.headers['x-mrest-time-0'];
    sdata.headers['x-mrest-pubhash-1'] = sdata2.headers['x-mrest-pubhash-0'];
    var mess = sdata.data.data;
    var signatures = auth.verify_message(mess, sdata.headers, [pubHash, pubHash2], 'get', [pubHash, pubHash2])
    signatures.should.have.property(pubHash);
    signatures[pubHash].should.equal(sdata.headers['x-mrest-sign-0']);
    signatures.should.have.property(pubHash2);
    signatures[pubHash2].should.equal(sdata.headers['x-mrest-sign-1']);
    done();
  });
  it('verify_message_multiple_unauthorized_required_signers', function(done) {
    var privKey2 = new bitcore.PrivateKey();
    var pubHash2 = privKey2.toAddress().toString();
    var privKey3 = new bitcore.PrivateKey();
    var pubHash3 = privKey3.toAddress().toString();
    var sdata = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash, privKey, {}, ['authenticate'])
    var sdata2 = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash2, privKey2, {}, ['authenticate'])
    sdata.headers['x-mrest-sign-1'] = sdata2.headers['x-mrest-sign-0'];
    sdata.headers['x-mrest-time-1'] = sdata2.headers['x-mrest-time-0'];
    sdata.headers['x-mrest-pubhash-1'] = sdata2.headers['x-mrest-pubhash-0'];
    var mess = sdata.data.data;
    ( function() {
        auth.verify_message(mess, sdata.headers, [pubHash, pubHash3], 'get', [pubHash, pubHash2])}
    ).should.throw('Unauthorized pubhash ' + pubHash2);
    done();
  });
  it('verify_message_multiple_authorized_unrequired_signers', function(done) {
    var privKey2 = new bitcore.PrivateKey();
    var pubHash2 = privKey2.toAddress().toString();
    var privKey3 = new bitcore.PrivateKey();
    var pubHash3 = privKey3.toAddress().toString();
    var sdata = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash, privKey, {}, ['authenticate'])
    var sdata2 = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash2, privKey2, {}, ['authenticate'])
    sdata.headers['x-mrest-sign-1'] = sdata2.headers['x-mrest-sign-0'];
    sdata.headers['x-mrest-time-1'] = sdata2.headers['x-mrest-time-0'];
    sdata.headers['x-mrest-pubhash-1'] = sdata2.headers['x-mrest-pubhash-0'];
    var mess = sdata.data.data;
    ( function() {
        auth.verify_message(mess, sdata.headers, [pubHash, pubHash2], 'get', [pubHash, pubHash3])}
    ).should.throw('expected 2 signatures but only found 1');
    done();
  });
  it('verify_message_multiple_authorized_no_required_signers', function(done) {
    var privKey2 = new bitcore.PrivateKey();
    var pubHash2 = privKey2.toAddress().toString();
    var privKey3 = new bitcore.PrivateKey();
    var pubHash3 = privKey3.toAddress().toString();
    var sdata = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash, privKey, {}, ['authenticate'])
    var sdata2 = auth.prepare_mrest_message('get', {'id': 'cupcakes'}, pubHash2, privKey2, {}, ['authenticate'])
    sdata.headers['x-mrest-sign-1'] = sdata2.headers['x-mrest-sign-0'];
    sdata.headers['x-mrest-time-1'] = sdata2.headers['x-mrest-time-0'];
    sdata.headers['x-mrest-pubhash-1'] = sdata2.headers['x-mrest-pubhash-0'];
    var mess = sdata.data.data;
    var signatures = auth.verify_message(mess, sdata.headers, [pubHash, pubHash2], 'get', [pubHash])
    signatures.should.have.property(pubHash);
    signatures[pubHash].should.equal(sdata.headers['x-mrest-sign-0']);
    done();
  });
});
