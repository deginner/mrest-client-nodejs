var fs = require('fs');
var path = require('path');
var should = require('should');
var mrest_client = require("../client.js");
var bitcore = require('bitcore');

// Default to localhost configuration and example host
var SERVER_PUB_KEY = "1F26pNMrywyZJdr22jErtKcjF8R3Ttt55G"

// The example schema included with the Flask MREST server
var COIN_SCHEMA = {
    "description": "model for coin",
    "title": "CoinSA",
    "required": [
        "metal",
        "mint"
    ],
    "routes": {
        "/:id": {
            "PUT": [
                "authenticate"
            ],
            "GET": [
                "authenticate"
            ],
            "DELETE": [
                "authenticate"
            ]
        },
        "/": {
            "POST": [
                "authenticate"
            ],
            "GET": [
                "authenticate"
            ]
        }
    },
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "properties": {
        "mint": {
            "type": "string",
            "maxLength": 255
        },
        "metal": {
            "type": "string",
            "maxLength": 255
        }
    },
    "signers": [SERVER_PUB_KEY]
}

// The coin to create as part of this test
var NEWCOIN = {"metal":"AU", "mint":"perth"};
var COIN_ID = 1;

// establish control group
// tests may fail first time you run them, as they don't wait for this
var PRIVATEKEY = new bitcore.PrivateKey.fromWIF("L4wr1My2icTdPnF2VGP5gni2qdpSPv9ZbmC1LsgPjhxftcaieibS");
var address = PRIVATEKEY.toAddress();
var cfg = {"url": "http://0.0.0.0:8002", "host": "example.com",
      "privkey": PRIVATEKEY,
      "schemas": {"coin": COIN_SCHEMA}, 
      "keyring": [SERVER_PUB_KEY]};
var MREST = mrest_client(cfg);
MREST.update_server_info({accept_keys: true}, function() {      
  MREST.post({model: 'user', data: {pubhash: MREST.pubhash, username: MREST.pubhash.substring(0,7)}}, function(err, resp) {
    // safe to ignore errors, probably
    MREST.post({model: 'coin', data: NEWCOIN}, function(err, resp) {
      COIN_ID = resp.id;
    });
  });
});

describe('mrest_client', function() {
  it('update_server_info', function(done) {
    var cfg = {"url": "http://0.0.0.0:8002", "host": "example.com",
              "privkey": PRIVATEKEY, 
              "keyring": [SERVER_PUB_KEY]};
    var mrest = mrest_client(cfg);
    mrest.update_server_info({'accept_unsigned': true}, function(err) {
      should.equal(err, undefined);
      mrest.routes.should.have.property('coin');
      mrest.routes.should.have.property('user');
      mrest.routes.coin.should.have.property('/');
      mrest.routes.coin.should.have.property('/:id');
      mrest.routes.user['/'].should.have.property('POST');
      mrest.routes.user['/:id'].should.have.property('GET');
      mrest.validator.getSchema("/coin").title.should.equal("CoinSA");
      done();
    });
  });
  it('update_server_info_do_not_accept_keys', function(done) {
    var cfg = {"url": "http://0.0.0.0:8002", "host": "example.com",
              "privkey": PRIVATEKEY,
              "schemas": {"coin": COIN_SCHEMA}, 
              "keyring": []};  // trust no one!
    var mrest = mrest_client(cfg);
    mrest.update_server_info({}, function(err){
      err.should.be.an.Error;
      err.toString().should.containEql("Error: expected 1 signatures but only found 0");
      done();
    });
  });
  it('update_server_info_accept_keys', function(done) {
    var cfg = {"url": "http://0.0.0.0:8002", "host": "example.com",
          "privkey": PRIVATEKEY,
          "schemas": {"coin": COIN_SCHEMA}, 
          "keyring": []};
    var mrest = mrest_client(cfg);
    mrest.update_server_info({accept_keys: true}, function() {      
      mrest.keyring.should.containEql(SERVER_PUB_KEY);
      done();
    });
  });
  it('get_authorized', function(done) {
    var cfg = {"url": "http://0.0.0.0:8002", "host": "example.com",
          "privkey": PRIVATEKEY,
          "schemas": {"coin": COIN_SCHEMA}, 
          "keyring": []};
    var mrest = mrest_client(cfg);
    mrest.update_server_info({accept_keys: true}, function() {      
      mrest.keyring.should.containEql(SERVER_PUB_KEY);
      done();
    });
  });
  it('register', function(done) {
    var privateKey2 = new bitcore.PrivateKey();
    var cfg = {"url": "http://0.0.0.0:8002", "host": "example.com",
          "privkey": privateKey2,
          "schemas": {"coin": COIN_SCHEMA}, 
          "keyring": []};
    var mrest = mrest_client(cfg);
    mrest.update_server_info({accept_keys: true}, function() {      
      mrest.post({model: 'user', data: {pubhash: mrest.pubhash, 
          username: mrest.pubhash.substring(0,7)}}, function(err, resp) {
        if (err != undefined) {
          return done(err);
        }
        should.exist(resp);
        resp.should.have.property('id');
        resp.should.have.property('pubhash');
        resp.pubhash.should.equal(mrest.pubhash);
        done();
      });
    });
  });
  it('post', function(done) {
    MREST.post({model: 'coin', data: NEWCOIN}, function(err, resp) {
      if (err != undefined) {
        return done(err);
      }
      should.exist(resp);
      resp.should.have.property('metal');
      resp.metal.should.equal(NEWCOIN.metal);
      resp.should.have.property('mint');
      resp.mint.should.equal(NEWCOIN.mint);
      resp.should.have.property('id');
      done();
    });
  });
  it('get all', function(done) {
    MREST.get({model: 'coin'}, function(err, resp) {
      if (err != undefined) {
        return done(err);
      }
      should.exist(resp);
      resp.should.be.instanceOf(Array);
      done();
    });
  });
  it('get by id', function(done) {
    MREST.get({model: 'coin', id: COIN_ID}, function(err, resp) {
      if (err != undefined) {
        return done(err);
      }
      should.exist(resp);
      resp.should.have.property('metal');
      resp.metal.should.equal(NEWCOIN.metal);
      resp.should.have.property('mint');
      resp.mint.should.equal(NEWCOIN.mint);
      resp.should.have.property('id');
      resp.id.should.equal(COIN_ID);
      done();
    });
  });
  it('put', function(done) {
    MREST.put({model: 'coin', id: COIN_ID,
               data: {"metal": "PD", "mint": "perth"}}, function(err, resp) {
      if (err != undefined) {
        return done(err);
      }
      should.exist(resp);
      resp.should.have.property('metal');
      resp.metal.should.equal("PD");
      resp.should.have.property('mint');
      resp.mint.should.equal(NEWCOIN.mint);
      resp.should.have.property('id');
      resp.id.should.equal(COIN_ID);
      done();
    });
  });
  it('delete', function(done) {
    MREST.delete({model: 'coin', id: COIN_ID}, function(err, resp) {
      if (err != undefined) {
        return done(err);
      }
      // response code should be 204 with no content
      done();
    });
  });
});
