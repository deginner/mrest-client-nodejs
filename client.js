'use strict';

var assert = require('assert')
var superagent = require('superagent');
var jsonschema = require('jsonschema');
var bitcore = require('bitcore');
var auth = require('./auth');

/**
 * Creates a new MREST client object
 * @name mrest_client
 * @constructor
 */
function mrest_client(cfg) {
  if (!(this instanceof mrest_client))
    return new mrest_client(cfg);

  this.routes = {};

  // load configuration
  assert(cfg, 'cfg is required');
  assert(cfg.url, 'cfg.url is required');
  this.url = cfg.url;
  assert(cfg.host, 'cfg.host is required');
  this.host = cfg.host;

  // load API credentials
  this.keyring = [];
  if (cfg.hasOwnProperty('privkey')) {
    this.privkey = new bitcore.PrivateKey.fromWIF(cfg.privkey);
    this.pubhash = this.privkey.toAddress().toString();
    this.keyring = [this.pubhash]  // start by trusting your own key
    if (cfg.hasOwnProperty('keyring'))
      this.keyring = this.keyring.concat(cfg['keyring']);
  }
  // Create a json schema validator
  this.validator = new jsonschema.Validator();
  // add any schemas present in configuration to the validator
  if (cfg.hasOwnProperty('schemas')) {
    for (var model in cfg.schemas) {
      this.add_schema(model, cfg.schemas[model])
    }
  }
}

/**
 * Prepare a request for sending to the server.
 * Use authentication if it is available and indicated by the schema.
 */
mrest_client.prototype.prepare_request = function(opts, cb) {
  if (!opts.hasOwnProperty('headers')) {
    opts.headers = {}
  }
  if (!opts.hasOwnProperty('model')) {
    opts.model = ""
  }
  var permissions = [];
  var url = this.url + "/" + opts.model;
  if (opts.model != "") {
    var route_rules = this.routes[opts.model] || {'/': {}, '/:id': {}};
    if (opts.hasOwnProperty('id')) {
      if (!(route_rules['/:id'].hasOwnProperty(opts.method))) {
        throw new Error("route and method not supported for this model");
      }
      url += "/" + opts.id;
      permissions = route_rules['/:id'][opts.method];
    } else {
      if (!(route_rules['/'].hasOwnProperty(opts.method))) {
        throw new Error("request method is not allowed");
      }
      permissions = route_rules['/'][opts.method];
    }
  }

  var method = opts.method.toLowerCase();
  var xmess = auth.prepare_mrest_message(method, opts.data, this.pubhash,
              this.privkey, opts.headers, permissions);
  xmess.url = url;

  return xmess;
};

/**
 * Send a request.
 */
mrest_client.prototype.send_request = function(optos, cb) {
  var xmess = this.prepare_request(optos);
  var _this = this;  // required to jump inside callback namespace...
  superagent(optos.method, xmess.url).set(xmess.headers).send(xmess.data)
    .end(function (err, response) {
      if (err) {
        _this._process_response(err, optos, cb);
      } else {
        _this._process_response(response, optos, cb);
      }
    });
};

mrest_client.prototype.add_schema = function(model, schema) {
  this.validator.addSchema(schema, "/" + model);
  this.routes[model] = schema['routes'];
}

mrest_client.prototype.add_schemas = function(schemas) {
  for (var model in schemas) {
    this.add_schema(model, schemas[model]);
  }
}

mrest_client.prototype.get_schema = function(model) {
  return this.validator.getSchema("/" + model);
}

/**
 * Calling the server root (/) to get its configuration details.
 * Using validate=False means that the response will not be validated 
 * against a json schema. If it is signed, the signature will still
 * be authenticated. This may create an Error.
 *
 * Callback is provided for syncronicity, but does not expect an argument.
 * Only Errors are passed to cb.
 */
mrest_client.prototype.update_server_info = function(opts, cb) {
  opts['validate'] = false;
  opts.model = "";
  // don't authenticate if we're going to accept the server's keys
  if (opts.hasOwnProperty('accept_keys') && opts.accept_keys)
    opts['auth'] = false;
  else if (opts.hasOwnProperty('accept_unsigned'))
    opts['auth'] = !(opts.accept_unsigned)
  var _this = this;  // required to jump inside callback namespace...
  this.get(opts, function(err, res) {
    if (err instanceof Error) {
      // An error here is probably critical, but let cb handle
      cb(err);
    } else {
      _this.raw_info = res;
      // all looks good. load the schemas.
      _this.add_schemas(res['schemas']);
      // accept the server's keys if indicated
      if (opts.hasOwnProperty('accept_keys') && opts.accept_keys) {
        _this.keyring = _this.keyring.concat(res['keyring']);
      }
      cb();
    }
  });
};

/**
 * Process a response to ensure no errors and body is valid.
 * Check signatures, if present and required.
 */
mrest_client.prototype._process_response = function(response, opts, cb) {
  if (response.status < 200 || response.status > 300) { // unexpected response code
    cb(new Error(opts.model + " " + response.method + "() Received unexpected response status " + response.status), response);
    opts = {};
  } else {
    var decoded_body = response.body
    if ((response.body instanceof Object && response.body.hasOwnProperty('data')) ||
        (typeof(response.body) == 'string' && response.body.indexOf('data') >= 0)) {
      decoded_body = auth.decode_signed_message(response.body);
      if (!(opts.hasOwnProperty('auth')) || opts['auth']) {
         var signers = [];
         if (opts.hasOwnProperty('model')) {
           if (opts.model == "") {
             signers.push(response.headers['x-mrest-pubhash-0']);
           }
           else {
             var schema = this.get_schema(opts.model);
             if (schema != undefined && schema.hasOwnProperty('signers')) {
               signers = schema['signers']
             }
           }
         }
         try {
           auth.verify_message(response.body['data'], response.headers, this.keyring, 'RESPONSE', signers);
         } catch(err) {
           cb(err);
         }
      }
    }
    if (opts.hasOwnProperty('validate') && opts.validate &&
        opts.hasOwnProperty('multi') && opts.multi &&
        decoded_body instanceof Array) {
      for (var resp in decoded_body) {
        this.validator.validate(resp, this.get_schema(opts.model));
      }
    } else if (opts.hasOwnProperty('validate') && opts.validate) {
      this.validator.validate(decoded_body, this.get_schema(opts.model));
    }
    cb(undefined, decoded_body);
    opts = {};
  }
};

/**
 * Make a GET request to a find an object
 */
mrest_client.prototype.get = function(opts, cb) {
  opts.method = 'GET';
  this.send_request(opts, cb);
};

/**
 * Make a POST request to create an object
 */
mrest_client.prototype.post = function(opts, cb) {
  var schema = this.get_schema(opts.model);
  if (schema == undefined) {
    cb(new TypeError("Schema not found for model " + opts.model), undefined)
  } else {
    this.validator.validate(opts.data, this.get_schema(opts.model))
    opts.method = 'POST';
    this.send_request(opts, cb);
  }
};

/**
 * Make a PUT request to update an object
 */
mrest_client.prototype.put = function(opts, cb) {
  var schema = this.get_schema(opts.model);
  if (opts.id == undefined || opts.data == undefined || opts.data.length == 0) {
    cb(new TypeError("invalid put request parameters"), undefined);
  }
  if (schema == undefined) {
    cb(new TypeError("Schema not found for model " + model), undefined);
  } else {
    opts.method = 'PUT';
    this.send_request(opts, cb);
  }
};

/**
 * Make a DELETE request to delete an object
 */
mrest_client.prototype.delete = function(opts, cb) {
  var schema = this.get_schema(opts.model);
  if (schema == undefined) {
    cb(new TypeError("Schema not found for model " + opts.model), undefined)
  } else {
    opts.method = 'DELETE';
    this.send_request(opts, cb);
  }
};

module.exports = mrest_client;

