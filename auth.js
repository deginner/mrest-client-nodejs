'use strict';

var assert = require('assert')
var jsonschema = require('jsonschema');
var bitcore = require('bitcore');
var bitmess = require('bitcore-message');


/**
 * Extract the mrest headers from a collection of headers.
 */
var extract_mrest_headers = function(headers) {
  var heads = {};
  for (var head in headers) {
    var hkey;
    var hval;
    if (typeof(head) == 'string') {
        hkey = head
        hval = headers[head]
    } else {
      hkey = head[0]
      hval = head[1]
    }
    if (hkey.indexOf('x-mrest') >= 0) {
      heads[hkey] = hval
    }
  }
  return heads
}


/**
 * Single authentication data creation function to ensure uniform format.
 */
var encode_signed_message = function(data) {
  if (data == undefined) {
    data = ""
  }
  if (!(typeof(data) == 'string') || (typeof(data) == 'string' && data.length == 0)) {
    data = JSON.stringify(data)
  }
  return {data: new Buffer(data).toString('base64')}
}


var decode_signed_message = function(data) {
  var ddata = data;
  if (typeof(data) == 'string') {
    ddata = JSON.parse(data);
  }
  if (ddata.hasOwnProperty('data')) {
    var jdata = new Buffer(ddata.data, 'base64').toString('ascii')
    ddata = JSON.parse(jdata)
  }
  return ddata;
}


/**
 * Pack the components that make up a normal MREST request into an object.
 * This object can then be sent as a single message,
 * for instance by websocket or message queue.
 */
var encode_compact_signed_message = function(method, data, headers, model, itemid) {
  var cdata;
  if (data instanceof Object && data.hasOwnProperty('data') &&
      Object.keys(data).length == 1) {
    cdata = data;
  } else if (typeof(data) == 'string' && data.indexOf('data') >= 0) {
    cdata = JSON.parse(data)
  } else {
    cdata = encode_signed_message(data)
  }
  cdata['headers'] = headers
  cdata['method'] = method
  cdata['model'] = model
  if (itemid != undefined) {
    cdata['id'] = itemid
  }
  return cdata
}


/**
 * Get a signed message from a compact signed message, losing the extra
 * meta data. Use this to prepare a compact signed message to be sent
 * using a client using a headers-separate protocol like HTTP.
 */
var strip_compact_signed_message = function(message) {
  var newmess = {}
  newmess['data'] = message['data']
  return newmess
}


/**
 * Single signing string creation function to ensure uniform format.
 */
var create_sign_str = function(data, method, auth_time) {
  return data + method.toUpperCase() + auth_time;
}


/**
 * Sign the message using the private key provided.
 */
var sign_message = function(data, method, auth_time, privkey) {
  var signstr = create_sign_str(data, method, auth_time)
  var signMess = new bitmess(signstr);
  return signMess.sign(privkey);
}


/**
 * Append the signature headers provided to the existing headers object.
 * New headers should be unnumbered (i.e. "x-mrest-sign"), and will
 * be given the next unused number.
 */
var add_signature_to_headers = function(mrest_headers, headers) {
  var exheaders;
  if (headers == undefined) {
    exheaders = {};
  } else {
    exheaders = extract_mrest_headers(headers);
  }
  var signum = 0;
  for (var head in exheaders) {
    if (head.indexOf('sign') >= 0) {
      var pastsignum = head.substring(13);
      // don't add signature if key has already signed
      if (mrest_headers['x-mrest-pubhash'] ==
          exheaders['x-mrest-pubhash-' + pastsignum]) {
        return headers;
      }
      if (parseInt(pastsignum) >= signum) {
        signum = pastsignum + 1;
      }
    }
  }
  for (var head in mrest_headers) {
    exheaders[head + "-" + signum] = mrest_headers[head]
  }
  return exheaders;
}


/**
 * Add all mrest headers, including authentication if permissions indicate so.
 */
var prepare_mrest_message = function(method, data, pubhash, privkey, headers, permissions) {
  if (headers == undefined)
    headers = {};
  if (data == undefined)
    data = "";
  if (permissions == undefined) {
    permissions = [];
  }
  method = method.toUpperCase()
  if (method == 'RESPONSE' || permissions.length > 0) {
    if (method == 'RESPONSE' || (permissions.indexOf("authenticate") >= 0 &&
        privkey != undefined)) {
      data = encode_signed_message(data);
      var datastr = data['data'];
      var auth_time = Date.now().toString();
      var sign = sign_message(datastr, method, auth_time, privkey);
      var tmpheaders = {
        'x-mrest-sign': sign,
        'x-mrest-time': auth_time,
        'x-mrest-pubhash': pubhash
      };
      headers = add_signature_to_headers(tmpheaders, headers);
    } else if (permissions.indexOf('pubhash') > -1) {
      headers['x-mrest-pubhash-0'] = pubhash
    }
  }
  return {headers: headers, data: data}
}


/**
 * Verify an mrest message is properly authenticated.
 */
var verify_message = function(data, headers, keyring, method, signers) {
  var signatures = Object()
  if (signers != undefined && signers.length > 0) {
    for (var head in headers) {
      if (!(headers.hasOwnProperty(head)))
        continue
      if (head.indexOf("x-mrest-pubhash-") >= 0) {
        if (keyring.indexOf(headers[head]) < 0) {
          throw new Error("Unauthorized pubhash " + headers[head]);
        }
        if (signers.indexOf(headers[head]) < 0) {
          continue  // signature is not required
        }
        var signnum = head.substring(16);
        var sign = headers['x-mrest-sign-' + signnum]
        var signdata = create_sign_str(data, method, headers['x-mrest-time-' + signnum])
        var Mess = new bitmess(signdata);
        var ph = headers['x-mrest-pubhash-' + signnum]
        if (!(Mess.verify(ph, sign)))
          throw new Error("Signature verification failed");
        signatures[ph] = sign
      }
    }
  }
  if (Object.keys(signatures).length != signers.length) {
    throw new Error("expected " + signers.length.toString() +
                    " signatures but only found " +
                    Object.keys(signatures).length.toString());
  }
  return signatures;
}


module.exports = {
  encode_compact_signed_message: encode_compact_signed_message,
  encode_signed_message: encode_signed_message,
  decode_signed_message: decode_signed_message,
  create_sign_str: create_sign_str,
  sign_message: sign_message,
  prepare_mrest_message: prepare_mrest_message,
  verify_message: verify_message
};

